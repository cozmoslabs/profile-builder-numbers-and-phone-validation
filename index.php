<?php
    /*
    Plugin Name: Profile Builder - Numbers and Phone Validation
    Plugin URI: http://www.cozmoslabs.com/wordpress-profile-builder/
    Description: Extends the functionality of Profile Builder by forcing an input to be numbers only (integer) OR a phone format (###) ### ####
    Version: 1.0.1
    Author: Cozmoslabs, Cristian Antohe
    Author URI: http://www.cozmoslabs.com/
    License: GPL2

    == Copyright ==
    Copyright 2014 Cozmoslabs (www.cozmoslabs.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
    */

    /*
     * Function that enqueues the necessary scripts
     *
     * @since v.1.0.0
     */
    function wppb_numbers_only_scripts() {
        wp_enqueue_script( 'wppb-numbers-only-field', plugin_dir_url(__FILE__) . 'assets/js/main.js', array( 'jquery', 'wppb-manage-fields-live-change' ) );
    }
    add_action( 'admin_enqueue_scripts', 'wppb_numbers_only_scripts' );

    /*
     * Function that adds the numbers only checkbox on an input field.
     *
     * @since v.1.0.0
     *
     * @param array $fields - The current field properties
     *
     * @return array        - The field properties that now include the numbers only checkbox
     */
    function wppb_numbers_only_field( $fields ) {
        $max_length_manage_field = array( 'type' => 'checkbox', 'slug' => 'numbers-only', 'title' => __( 'Numbers Only', 'profilebuilder' ), 'options' => array( 'Yes' ), 'description' => __( "Only numbers are allowed in this input field.", 'profilebuilder' ) );
        array_push( $fields, $max_length_manage_field );

		$max_length_manage_field = array( 'type' => 'checkbox', 'slug' => 'phone-format', 'title' => __( 'Phone Format', 'profilebuilder' ), 'options' => array( 'Standard' ), 'description' => __( "Requires a phone format (###)### - ####", 'profilebuilder' ) );
		array_push( $fields, $max_length_manage_field );

        return $fields;
    }
    add_filter( 'wppb_manage_fields', 'wppb_numbers_only_field' );

    /*
     * Function modifies the field validation so we're taking into account if they should be numbers only or phone format.
     *
     * @since v.1.0.0
     *
     * @param int $default_value
     * @param array $field
     *
     * @return int
     */

	function wppb_check_input_numeric_value( $message, $field, $request_data, $form_location ){
		//var_dump( $field );
		if( $field['field'] == 'Input' ){
			if ( ( isset( $request_data[$field['meta-name']] ) && ( trim( $request_data[$field['meta-name']] ) == '' ) ) && ( $field['required'] == 'Yes' ) ){
				return wppb_required_field_error($field["field-title"]);
			}

			if ( ( isset( $request_data[$field['meta-name']] ) && ( trim( $request_data[$field['meta-name']] ) != '' )  &&  ( $field['numbers-only'] == 'Yes' ) ) ){
				//var_dump( $request_data );
				if ( ! ctype_digit( $request_data[$field['meta-name']] ) ){
					return __( 'Please enter numbers only.', 'profilebuilder' );
				}
			}

			if ( ( isset( $request_data[$field['meta-name']] ) && ( trim( $request_data[$field['meta-name']] ) != '' )  &&  ( $field['phone-format'] == 'Standard' ) ) ){
				//var_dump( $request_data );
				if ( !  preg_match('/^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$/', $request_data[$field['meta-name']], $matches) ){
					return __( 'Phone format: (###)###-####', 'profilebuilder' );
				}
			}

		}

		return $message;
	}
	add_filter( 'wppb_check_form_field_input', 'wppb_check_input_numeric_value', 12, 4 );
?>