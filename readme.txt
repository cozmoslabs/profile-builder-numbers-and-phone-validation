=== Profile Builder - Numbers Only Field ===

Contributors: cozmoslabs, sareiodata
Donate link: http://www.cozmoslabs.com/wordpress-profile-builder/
Tags: registration, profile, user registration, custom field registration, customize profile, user fields, builder, profile builder, custom profile, user profile, custom user profile, user profile page, 
custom registration, custom registration form, custom registration page, extra user fields, registration page, user custom fields, user listing, user login, user registration form, front-end login, 
front-end register, front-end registration, frontend edit profile, edit profileregistration, customize profile, user fields, builder, profile builder, custom fields, avatar
Requires at least: 3.1
Tested up to: 4.0
Stable tag: 1.0.0
Requires Profile Builder at least: 2.0.5


Extends the functionality of Profile Builder by allowing you to check if a field contains numbers only.


== Description ==

Profile Builder - Numbers Only Field

Extends the functionality of Profile Builder by allowing you to check if a field contains numbers only.

NOTE:

This plugin is compatible with Profile Builder version 2.0.5 or higher
	


== Installation ==

1. Upload the profile-builder-numbers-only-field folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress


== Screenshots ==
1. Manage Fields: screenshot1.jpg
2. Front end error: screenshot2.jpg