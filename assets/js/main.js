/**
 * Function that adds the numbers and phone field properties to the global fields object
 * declared in assets/js/jquery-manage-fields-live-change.js
 *
 */
function numbers_and_phone_updateFields() {
    if (typeof fields == "undefined") {
        return false;
    }

    var updateFields = ['Input'];

    for( var i = 0; i < updateFields.length; i++ ) {
        fields[ updateFields[i] ]['show_rows'].push( '.row-numbers-only' );
        fields[ updateFields[i] ]['show_rows'].push( '.row-phone-format' );
    }
}

jQuery( function() {
	numbers_and_phone_updateFields();
});